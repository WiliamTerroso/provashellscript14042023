#! /bin/bach 
echo -e 1. O QUE É UMA VARIÁVEL?
echo -e Uma variável, como o próprio nome já diz, serve para armazenar conteúdos variados em 
echo -e um local/espaço da memória, ao invés de utilizarmos números para fazer a sua chamada, 
echo -e usamos tags/nomes. Assim fica mais fácil fazer a chamada não é? Variáveis em shell podem receber qualquer conteúdo! 
echo -e Desde uma simples string/texto, números, status de saída de um comando e retorno da saída.
echo -e QUANDO DEVEMOS USAR UMA VARIÁVEL?
echo -e Em qualquer momento variáveis são legais de se trabalhar, 
echo -e vamos supor como exemplo que eu precisasse fazer a chamada de um diretório específico 4 vezes.
echo -e Seria viável eu ficar digitando sempre esse diretório manualmente? 
echo -e Não seria falho se em um futuro eu resolvesse alterar o diretório em si? 
echo -e se eu esquecer de alterar algum caminho é falha na certa! Para isto eu uso uma variável para me ajudar! 
echo -e

