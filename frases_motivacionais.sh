#! /bin/bash
echo -e "\e[1;36m Não importa que você vá devagar,  \e[0m"
sleep 1
echo -e "\e[1;32m contanto que você não pare. \e[0m"
sleep 1
echo -e "\e[1;31m use o que você tem  \e[0m"
sleep 1
echo -e "\e[1;33m e faça o que você pode. \e[0m"
sleep 1
echo -e "\e[1;34m Devíamos ser ensinados  \e[0m"
#
echo -e "\e[1;36m a não esperar por inspiração   \e[0m"
sleep 1
echo -e "\e[1;32m para começar algo. \e[0m"
sleep 1
echo -e "\e[1;33m Ação sempre gera inspiração. \e[0m"
sleep 1
echo -e "\e[1;34m Inspiração raramente gera ação.  \e[0m"
